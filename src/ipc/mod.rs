use serde::{Deserialize, Serialize};
use std::{
    fmt,
    path::{Path, PathBuf},
    process::Command,
};

use crate::parameters::DaemonType;
use crate::{bad, config, outcome::Outcome, parameters::Parameters, time};

mod mqtt;
#[cfg(unix)]
mod unix;
#[cfg(windows)]
mod windows;

pub use mqtt::MqttClient;

#[allow(unused_variables)]
pub fn daemon(func: fn(&Parameters) -> Outcome<()>, params: &Parameters) -> Outcome<()> {
    #[cfg(unix)]
    {
        unix::daemon(func, &params)
    }
    #[cfg(windows)]
    {
        match params.daemon_type {
            DaemonType::WindowsClient => {
                windows::redirect_stdio_to_null()?;
                crate::client::init(params)
            }
            DaemonType::WindowsServer => {
                windows::redirect_stdio_to_null()?;
                crate::server::init(params)
            }
            // not daemonized yet
            _ => windows::daemon().map(|_pid| ()),
        }
    }
}

pub type Rx = paho_mqtt::Receiver<Option<paho_mqtt::Message>>;

#[derive(PartialEq, Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Reason {
    Busy,   // server will enter this state
    Behind, // response to client, never enters state
    Other,
}

#[derive(PartialEq, Default, Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Status {
    NotReady(Reason),
    #[default]
    Ready,
}

impl fmt::Display for Status {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Status::NotReady(reason) => {
                write!(f, "NotReady(").unwrap();
                match reason {
                    Reason::Busy => write!(f, "Sinking").unwrap(),
                    Reason::Behind => write!(f, "Behind").unwrap(),
                    Reason::Other => write!(f, "Other").unwrap(),
                };
                write!(f, ")") // return result of write
            }
            Status::Ready => write!(f, "Ready"),
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Payload {
    pub hostname: String,
    pub username: String,
    pub src_paths: Vec<PathBuf>,
    pub dest_path: PathBuf,
    pub date: String,
    pub cycle: u32,
    pub status: Status,
}

#[allow(dead_code)]
impl Payload {
    pub fn new() -> Outcome<Payload> {
        Ok(Payload {
            hostname: config::get_hostname()?,
            username: config::get_username()?,
            src_paths: vec![],
            date: String::from("2022Jan4"),
            cycle: 0,
            status: Status::Ready,
            dest_path: PathBuf::from("server"),
        })
    }

    pub fn from(
        hostname: String,
        username: String,
        src_paths: Vec<PathBuf>,
        dest_path: PathBuf,
        date: String,
        cycle: u32,
        status: Status,
    ) -> Payload {
        Payload {
            hostname,
            username,
            src_paths,
            dest_path,
            date,
            cycle,
            status,
        }
    }

    pub fn hostname<S: Into<String>>(mut self, hostname: S) -> Self {
        self.hostname = hostname.into();
        self
    }

    pub fn username<S: Into<String>>(mut self, username: S) -> Self {
        self.username = username.into();
        self
    }

    pub fn src_paths(mut self, paths: Vec<PathBuf>) -> Self {
        self.src_paths = paths;
        self
    }

    pub fn date<S: Into<String>>(mut self, date: S) -> Self {
        self.date = date.into();
        self
    }

    pub fn cycle(mut self, cycle: u32) -> Self {
        self.cycle = cycle;
        self
    }

    pub fn status(mut self, status: &Status) -> Self {
        self.status = *status;
        self
    }

    pub fn dest_path<P: AsRef<Path>>(mut self, dest_path: P) -> Self {
        self.dest_path = PathBuf::from(dest_path.as_ref());
        self
    }
}

impl fmt::Display for Payload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "hostname: {}, username: {}, src_paths: [",
            self.hostname, self.username,
        )
        .unwrap();
        for path in &self.src_paths {
            write!(f, "{}, ", path.display()).unwrap();
        }
        write!(
            f,
            "], dest_path: {}, date: {}, cycle: {}, status: {}",
            self.dest_path.display(),
            self.date,
            self.cycle,
            self.status
        )
    }
}

pub fn encode(payload: &mut Payload) -> Result<Vec<u8>, paho_mqtt::Error> {
    payload.date = time::stamp(Some("%Y%m%d"));
    match bincode::serialize(payload) {
        Err(e) => Err(paho_mqtt::Error::GeneralString(format!(
            "FATAL, bincode::serialize >> {e}"
        ))),
        Ok(stream) => Ok(stream),
    }
}

pub fn decode(bytes: &[u8]) -> Result<Payload, paho_mqtt::Error> {
    match bincode::deserialize(bytes) {
        Err(_) => Err(paho_mqtt::Error::General(
            "FATAL, bincode could not deserialize",
        )),
        Ok(payload) => Ok(payload),
    }
}

fn resolve_host(host: Option<&str>) -> Result<String, paho_mqtt::Error> {
    match host {
        Some(h) if h.starts_with('/') => Err(paho_mqtt::Error::General(
            "Invalid hostname: it looks like a path. Did you mean 'localhost'?",
        )),
        Some(h) => {
            let fq_host = format!("tcp://{}:1883", h);
            debug!("Fully qualified host: {}", fq_host);
            Ok(fq_host)
        }
        None => Err(paho_mqtt::Error::General(
            "Host string is required but missing",
        )),
    }
}

pub fn start_mosquitto() -> Outcome<()> {
    debug!(">> spawn mosquitto daemon");
    let mut cmd = Command::new("mosquitto");

    #[cfg(unix)]
    cmd.arg("-d");

    if let Err(spawn_error) = cmd.spawn() {
        return bad!(format!(
            "Is mosquitto installed and in path? >> {}",
            spawn_error
        ));
    }
    Ok(())
}
